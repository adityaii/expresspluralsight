var EventEmitter = require('events').EventEmitter;

var getResource = function(c) {
    var e = new EventEmitter();
    process.nextTick(function() {
        var count = 0;
        e.emit('Start');
        var t = setInterval(function() {
            e.emit('data', ++count);
            console.log(count);
            if (count === c ) {
                e.emit('End', count);
                clearInterval(t);
            }
        });
    }, 10);
    return(e);
}

var r = getResource(5);

r.on('Start', function() {
    console.log("I've started");
});

r.on('data', function(d) {
    console.log("   I received data: " + d);
});

r.on('end', function(t) {
    console.log("I'm done with " + t + " data events.");
});
