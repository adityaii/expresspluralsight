var Resource = require('./resource')
var EventEmitter = require('events').EventEmitter;

var r = new Resource(7);

r.on('Start', function() {
    console.log("I've started");
});

r.on('data', function(d) {
    console.log("   I received data: " + d);
});

r.on('end', function(t) {
    console.log("I'm done with " + t + " data events.");
});
