var util = require('util');
var EventEmitter = require('events').EventEmitter;

function Resource(m) {
    var maxEvents = m;
    var self = this;
    process.nextTick(function() {
        var count = 0;
        self.emit('Start');
        var t = setInterval(function() {
            self.emit('data', ++count);
            console.log(count);
            if (count === maxEvents ) {
                self.emit('End', count);
                clearInterval(t);
            }
        });
    }, 10);


};

util.inherits(Resource, EventEmitter);

module.exports = Resource;
