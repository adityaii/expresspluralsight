var request = require('request');
var url = require('url');
var express = require('express');
var app = express();

app.get(
    '/job/:job_name', function(req, response) {
        var job_name = req.params.job_name;

        options = {
            protocol: "https",
            host: 'twinpeaks-jenkins.bsc.bscal.com/job/' + job_name,
            pathname: '/api/json',
            query: { tree: 'displayName' }
        }
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
        var twinpeaksUrl = url.format(options);
        // process.stdout.write(twinpeaksUrl);
        request(url, function(err, res, body) {
            // process.stdout.write(String(body));
            //  var jsonString = JSON.parse(body);
            //  process.stdout.write(jsonString);
            response.locals = {displayName: job_name}
            response.render('job.ejs');
        });
    }
);


// app.get(
//     '/job/:job_name', function(req, response) {
//         var job_name = req.params.job_name;

//         options = {
//             protocol: "https",
//             host: 'twinpeaks-jenkins.bsc.bscal.com/job/' + job_name,
//             pathname: '/api/json',
//             query: { tree: 'displayName' }
//         }
//         process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
//         var twinpeaksUrl = url.format(options);
//         process.stdout.write(twinpeaksUrl);
//         request(twinpeaksUrl).pipe(response);
//     }
// );

app.listen(8080);
